<?php

use App\Models\Movie;
use Tests\TestCase;

class MovieTest extends TestCase
{
    protected string $accountId = "ojowqiej1123n198enaisd";

    protected string $movieId = "qwejqpowejqoiweqwieqe";

    protected array $movieStructure =  [ 
        "id",
        "account_id",
        "published",
        "cast" => [
            'directors',
            'actors'
        ],
        "original_title",
        "production_year",
        "release_date",
        "video_id",
        "poster",
        "created_at",
        "updated_at",
    ];
    /**
     * /ott/v1/accId/movies [GET]
     */
    public function testShouldReturnAllProducts(){

        $this->get("/ott/v1/".$this->accountId."/movies", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'current_page',
            'data' => ['*' => $this->movieStructure],
            "next_page_url",
            "path",
            "per_page",
            "prev_page_url",
            "to",
            "total"
        ]);
        
    }

    /**
     * /ott/v1/accId/movies/moviesId [GET]
     */
    public function testShouldReturnProduct(): void
    {
        $movie = Movie::factory()->create(['account_id' => $this->accountId]);
        $this->get("/ott/v1/".$this->accountId."/movies/". $movie->id, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure($this->movieStructure);
    }

    /**
     * /ott/v1/accId/movies/ [POST]
     */
    public function testShouldCreateProduct(): void
    {

        $parameters = json_decode('{
            "published": 1,
            "production_country": [
                  "Portugal", "Spain"
            ],
            "cast": {
              "actors": ["Prof. Shad Mueller", "Kevon Herman", "Annabell Lind"],
              "directors": ["Dr. Liana Skiles", "Prof. Eryn Halvorson DDS"]
            },
            "original_title": "Avengers Endgame",
            "production_year": "2020",
            "release_date": "2022-12-25",
            "video_id": "1680608156245230581",
            "poster": "http://localhost:3000/moviess/1.jpg"
          }', true);

        $this->post("/ott/v1/".$this->accountId."/movies", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure($this->movieStructure);
        
    }
    
    /**
     * /ott/v1/accId/movies/moviesId [PUT]
     */
    public function testShouldUpdateProduct(): void
    {
        $movie = Movie::factory()->create(['account_id' => $this->accountId]);
        $parameters = json_decode('{
            "published": 1,
            "production_country": [
                  "Portugal", "Spain"
            ],
            "cast": {
              "actors": ["Prof. Shad Mueller", "Kevon Herman", "Annabell Lind"],
              "directors": ["Dr. Liana Skiles", "Prof. Eryn Halvorson DDS"]
            },
            "original_title": "Avengers Endgame",
            "production_year": "2020",
            "video_id": "1680608156245230581",
            "poster": "http://localhost:3000/moviess/1.jpg"
          }', true);


        $this->put("/ott/v1/".$this->accountId."/movies/". $movie->id, $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure($this->movieStructure);
        $resp = json_decode($this->response->getContent());
        $this->assertEquals($movie->id, $resp->id);
        $this->assertNotEquals($movie->original_title, $resp->original_title);
    }

    /**
     * /ott/v1/accId/movies/moviesId [DELETE]
     */
    public function testShouldDeleteProduct(): void
    {
        $movie = Movie::factory()->create(['account_id' => $this->accountId]);
        $this->delete("/ott/v1/".$this->accountId.'/movies/'.$movie->id , [], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
                'message'
        ]);
    }

}
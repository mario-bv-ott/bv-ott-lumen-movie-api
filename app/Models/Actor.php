<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Traits\ScopesByName;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actor extends BaseModel
{
    use HasUuids, HasFactory, ScopesByName, SoftDeletes;

    /**
     * table variable - table override name
     *
     * @var string
     */
    protected $table  = "actor";

    /**
     * fillable variable - fillable fields
     *
     * @var array
     */
    protected $fillable = 
    [
        'name',
        'birth_date',
        'gender',
        'email',
        'career_start_date',
        'career_end_date'
    ];

    /**
     * Movies function - Relationship M..N with movies
     *
     * @return BelongsToMany
     */
    public function movies(): BelongsToMany
    {
        return $this->belongsToMany(Movie::class, 'actor_movie');
    }
}
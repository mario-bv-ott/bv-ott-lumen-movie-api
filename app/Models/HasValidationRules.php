<?php

namespace App\Models;

interface HasValidationRules 
{
    public static function getValidationRules():array;
}
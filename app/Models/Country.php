<?php

namespace App\Models;

use App\Traits\ScopesByName;
use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel
{
    use UsesUUID, ScopesByName, SoftDeletes;

    /**
     * table variable - table override name
     *
     * @var string
     */
    protected $table  = "country";

    /**
     * fillable variable - fillable fields
     *
     * @var array
     */
    protected $fillable = 
    [
        'name',
        'code_a2',
        'code_a3',
    ];

    /**
     * Movies function - Relationship M..N With Movies
     *
     * @return BelongsTo
     */
    public function movies(): BelongsToMany
    {
        return $this->belongsToMany(Movie::class, 'country_movie');
    }
}
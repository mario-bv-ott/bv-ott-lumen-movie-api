<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\BaseModel;
use App\Models\Country;
use App\Models\Director;
use App\Models\Movie;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Exists;

class MovieController extends BaseController
{
    /**
     * All function - Gets All Movies based on one accountId
     *
     * @param string $accountId
     * @param integer $limit
     * @param integer $page
     * @return JsonResponse
     */
    public function all(string $accountId, int $limit = BaseModel::DEFAULT_LIMIT, int $page = 1): JsonResponse
    {
        $movies = Movie::accountId($accountId)
                ->skip($page * $limit)
                ->paginate($limit);

        return response()->json($movies);
    }

    /**
     * Show function - gets a record for a specific accountID Movie
     *
     * @param string $accountId
     * @param string $movieId
     * @return JsonResponse
     */
    public function show(string $accountId, string $movieId): JsonResponse
    {
        $movie = $this->findOrFailMovie($accountId, $movieId);
        return response()->json($movie);
    }

    /**
     * Store function - Creates/ a Movie
     *
     * @param string $accountId
     * @param Request $request
     * @return JsonResponse
     */
    public function store(string $accountId, Request $request): JsonResponse
    {
        $reqData = $request->only(
            [
            'published',
            'production_country',
            'cast',
            'original_title',
            'production_year',
            'video_id',
            'poster',
            'release_date'
            ]
        );
        
        $validator = validator($reqData, Movie::getValidationRules());
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $reqData['account_id'] = $accountId;
        $assignedRelations = 
        [
            'productionCountries' => null,
            'directors'           => null,
            'actors'              => null
        ];

        if (!empty($reqData['production_country'])) {
            $assignedRelations['productionCountries'] = Country::Names($reqData['production_country'])->get();
        }

        if (!empty($reqData['cast']['directors'])) {
            $assignedRelations['directors'] = Director::Names($reqData['cast']['directors'])->get();
        }

        if (!empty($reqData['cast']['actors'])) {
            $assignedRelations['actors'] = Actor::Names($reqData['cast']['actors'])->get();
        }

        $movie = New Movie($reqData);

        if (!$movie->save()) {
            throw new Exception('An error occurred trying to create new Movie', 500);
        }

        foreach ($assignedRelations as $rel => $values) {
            if (!empty($values)) {
                $movie->$rel()->attach($values);
            }
        } 
      
        return response()->json($movie, 201);
    }

    /**
     * Update function - Updates a Movie 
     * (I'd use only store method, but lumen routing doesn't permit it)
     *
     * @param string $accountId
     * @param Request $request
     * @return JsonResponse
     */
    public function update(string $accountId, string $movieId = null, Request $request): JsonResponse
    {
        $movie = $this->findOrFailMovie($accountId, $movieId);
        $reqData = $request->only(
            [
            'published',
            'production_country',
            'cast',
            'original_title',
            'production_year',
            'video_id',
            'poster',
            'release_date'
            ]
        );
        
        $validator = validator($reqData, Movie::getValidationRules());
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $assignedRelations = 
        [
            'productionCountries' => null,
            'directors'           => null,
            'actors'              => null
        ];

        if (!empty($reqData['production_country'])) {
            $assignedRelations['productionCountries'] = Country::Names($reqData['production_country'])->get();
        }

        if (!empty($reqData['cast']['directors'])) {
            $assignedRelations['directors'] = Director::Names($reqData['cast']['directors'])->get();
        }

        if (!empty($reqData['cast']['actors'])) {
            $assignedRelations['actors'] = Actor::Names($reqData['cast']['actors'])->get();
        }
        
        if (!$movie->update($reqData)) {
            throw new Exception('An error occurred trying to update existing new Movie', 500);
        }

        foreach ($assignedRelations as $rel => $values) {
            if (!empty($values)) {
                //being a put request, all related data is reinserted
                $movie->$rel()->syncWithoutDetaching($values);
            }
        }
      
        return response()->json($movie, 200);
    }

    /**
     * destroy function  Removes specific Movie for given AccountID
     *
     * @param string $accountId
     * @param string $movieId
     * @param [type] $id
     * @return JsonResponse
     */
    public function destroy(string $accountId, string $movieId): JsonResponse
    {
        $movie = $this->findOrFailMovie($accountId, $movieId);

        if (!$movie->delete()) {
            throw new Exception("An internal error occurred trying to delete given movie", 500);
        }

        return response()->json(["message" => "Movie deleted"], 200);
    }

    /**
     * FindOrFailMovie function - Wrapper for getting or failing 
     * getting specific movie for given account ID
     *
     * @param string $accountId
     * @param string $movieId
     * @return Movie
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    protected function findOrFailMovie(string $accountId, string $movieId): Movie
    {
        return Movie::accountId($accountId)
        ->findOrFail($movieId);
    }
}
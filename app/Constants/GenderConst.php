<?php

namespace App\Constants;

/**
 * GenderConst class - Class holding gender constants
 */
class GenderConst
{
    /**
     *  Const Value for Gender `Male`
     * @var string
     */
    public const GENDER_MALE = "M";

    /**
     *  Const Value for Gender `Male`
     * @var string
     */
    public const GENDER_FEMALE = "F";

    /**
     *  Const Value for Gender `Male`
     * @var string
     */
    public const GENDER_OTHER = "X";

    /**
     *  Collection of Genders in Platform
     * @var array
     */
    public const GENDERS = [
        self::GENDER_MALE,
        self::GENDER_FEMALE,
        self::GENDER_OTHER
    ];
}

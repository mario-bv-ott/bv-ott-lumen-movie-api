<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'ott/v1'], function () use ($router) {
    $router->get('/{accountId}/movies','MovieController@all');
    $router->get('{accountId}/movies/{movieId}', 'MovieController@show');
    $router->post('{accountId}/movies', 'MovieController@store');
    $router->put('{accountId}/movies/{movieId}', 'MovieController@update');
    $router->delete('{accountId}/movies/{movieId}', 'MovieController@destroy');
});
<?php

namespace Database\Factories;

use App\Models\Movie;

class MovieFactory extends BaseFactory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return 
        [
            'original_title'  => $this->faker->words($this->faker->randomDigitNotNull() , true),
            'published'       => $this->faker->boolean(),
            'release_date'    => $this->faker->boolean() ? $this->faker->date() : null,
            'production_year' => $this->faker->year(),
            'video_id'        => $this->faker->uuid(),
            'account_id'      => $this->faker->uuid(),
            'poster'          => $this->faker->url(),
        ];
    }
}

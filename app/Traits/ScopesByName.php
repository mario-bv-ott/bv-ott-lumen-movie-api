<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ScopesByName
{
    /**
     * Name function - scope by Name
     *
     * @param Builder $query
     * @param string $name
     * @return Builder
     */
    public function scopeName(Builder $query, string $name): Builder
    {
        return $query->where($this->getTable(). '.name', $name);
    }

    /**
     * Names function - scope by Names
     *
     * @param Builder $query
     * @param string $name
     * @return Builder
     */
    public function scopeNames(Builder $query, array $names): Builder
    {
        return $query->whereIn($this->getTable(). '.name', $names);
    }
}
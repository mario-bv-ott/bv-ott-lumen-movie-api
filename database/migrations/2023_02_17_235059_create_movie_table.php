<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('movie', function (Blueprint $table) {
            $table->uuid('id')->primary('id');

            // if a account table was created, a fk constraint would be here instead
            $table->uuid('account_id')->index('account_id');
            $table->boolean("published")->default(0);
            $table->string('original_title');
            $table->string('production_year', 4);
            $table->date('release_date')->nullable();
            $table->string('video_id');
            $table->index('video_id');
            $table->string('poster');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('movie');
    }
};

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * BaseModel class - Base Abstract Class for all child models
 * to extend.
 */
abstract class BaseModel extends Model
{
    /**
     * Default limit applied on Models
     * @var integer
     */
    public const DEFAULT_LIMIT = 50;

    public static function getTableName(): string
    {
        return (new static)->table;
    }

    /**
     * scopeUUID function - Scope on UUID
     *
     * @param Builder $query
     * @param string $uuid
     * @return Builder
     */
    public function scopeUUID(Builder $query, string $uuid): Builder 
    {
        return $query->where($this->getTable().'.'. $this->getKeyName(), $uuid);
    }
}
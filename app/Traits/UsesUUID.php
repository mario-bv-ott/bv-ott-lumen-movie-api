<?php

namespace App\Traits;

use App\Helpers\UUIDUtils;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait UsesUUID - Apply changes so that Models
 * can use & generate UUID's on creation
 */
trait UsesUUID 
{
    /**
     * Boot function - override boot function to inject 
     * generation of UUID's before inserting
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), UUIDUtils::generateUUID());
        });
    }

    /**
     * GetIncrementing function - override getIncrementing method
     * to disable it, since we're dealing with String generated UUID's
     *
     * @return boolean
     */
    public function getIncrementing(): bool
    {
        return false;
    }

    /**
     * GetKeyType function - overrides default key type to 
     * return string.
     *
     * @return void
     */
    public function getKeyType()
    {
        return 'string';
    }
}

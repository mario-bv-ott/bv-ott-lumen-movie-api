<?php

use App\Models\Actor;
use App\Models\Country;
use App\Models\Director;
use App\Models\Movie;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $movies = Movie::factory(30)->create();
        $directors = Director::factory(10)->create();
        $actors = Actor::factory(25)->create();
        $countries = Country::all();

        foreach ($movies as $movie) {
            $movie->productionCountries()->attach(
                $countries->random(rand(1, 3))->pluck('id')->toArray()
            ); 
            $movie->directors()->attach(
                $directors->random(rand(1, 3))->pluck('id')->toArray()
            ); 
            $movie->actors()->attach(
                $actors->random(rand(1, 3))->pluck('id')->toArray()
            ); 
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
};

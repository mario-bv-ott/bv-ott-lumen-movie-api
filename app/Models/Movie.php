<?php

namespace App\Models;

use App\Models\HasValidationRules;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rules\Exists;
use \stdClass;

class Movie extends BaseModel implements HasValidationRules
{
    use HasUuids, HasFactory, SoftDeletes;

    /**
     * table variable - table override name
     *
     * @var string
     */
    protected $table  = "movie";

    /**
     * fillable variable - fillable fields
     *
     * @var array
     */
    protected $fillable = 
    [
        'published',
        'original_title',
        'production_year',
        'release_date',
        'video_id',
        'account_id',
        'poster'
    ];

      /**
       * Boot function - Boot override for adding
       * additional steps on certain model events
       *
       * @return void
       */
      public static function boot() {
        parent::boot();
        /**
         * On deletion, releases all attached relation entries
         */
        static::deleting(function(Movie $movie) {
            $movie->actors()->detach();
            $movie->directors()->detach();
            $movie->productionCountries()->detach();
        });
    }

    /**
     * GetValidationRules function - returns validation rules
     * for movie entries
     *
     * @return array
     */
    public static function getValidationRules(): array
    {
        return 
        [
            'published'            => ['required', 'boolean'],
            'release_date'         => ['nullable', 'date'],
            'cast'                 => ['sometimes'],
            'cast.actors'          => ['sometimes', 'array'],
            'cast.directors'       => ['sometimes', 'array'],
            'production_country.*' => ['required', 'string', (new Exists(Country::getTableName(), 'name'))->whereNull('deleted_at')],
            'cast.actors.*'        => ['required', 'string', (new Exists(Actor::getTableName(), 'name'))->whereNull('deleted_at')],
            'cast.directors.*'     => ['required', 'string', (new Exists(Director::getTableName(), 'name'))->whereNull('deleted_at')],
            'original_title'       => ['required', 'string', 'max:255'],
            'production_year'      => ['required', 'integer', 'min:1900', 'max:'.(date('Y') + 20), 'digits:4'],
            'video_id'             => ['required', 'string', 'max:255'],
            'poster'               => ['required', 'string', 'url'],
        ];
        
    }

    /**
     * Appends variable - Appends cast acessor on movie model response
     *
     * @var array
     */
    protected $appends = ['cast'];

    /**
     * Actors function - Relation M..N with actor
     *
     * @return BelongsToMany
     */
    public function actors(): BelongsToMany
    {
        return $this->belongsToMany(Actor::class, 'actor_movie');
    }

    /**
     * Directors function - Relation M..N with directors
     *
     * @return BelongsToMany
     */
    public function directors(): BelongsToMany
    {
        return $this->belongsToMany(Director::class, 'director_movie');
    }

    /**
     * ProductionCountries function - Relation M..N with country
     *
     * @return HasMany
     */
    public function productionCountries(): BelongsToMany
    {
        return $this->belongsToMany(Country::class, 'country_movie');
    }

    /**
     * GetCastAttribute function - Define an acessor to
     * join actors and directors into a cast attribute.
     * Can later on be converted into a 1-to-1 table with movies,
     * where actors/directors etc are placed with their cast types.
     *
     * @return stdClass
     */
    protected function getCastAttribute(): stdClass
    {
        $cast            = new stdClass();
        $cast->actors    = $this->actors;
        $cast->directors = $this->directors;
        return  $cast;
    }

    /**
     * ScopeAccountId function - scope on Account ID
     *
     * @param Builder $query
     * @param string $accountId
     * @return Builder
     */
    public function scopeAccountId(Builder $query, string $accountId): Builder
    {
        return $query->where($this->table.'.account_id', $accountId);
    }
}
FROM php:8.2-fpm-alpine

# We add pdo_mysql extension to communicate with our database
RUN docker-php-ext-install pdo_mysql

# We add these dependencies to have pecl(PHP Extension Library) installed
# so that we can install phpredis extension. In this service, redis is used has lumen's cache driver
RUN apk add --no-cache pcre-dev $PHPIZE_DEPS \
        && pecl install redis \
        && docker-php-ext-enable redis.so

WORKDIR /var/www/html/

# installation of composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

#copy of directory to WORKDIR
COPY . .

RUN cp .env.example .env
RUN cp entry.sh /tmp/entry.sh
RUN chmod +x /tmp/entry.sh

RUN composer install 

RUN composer dump-autoload -o
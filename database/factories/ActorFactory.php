<?php

namespace Database\Factories;

use App\Constants\GenderConst;
use App\Models\Actor;

class ActorFactory extends BaseFactory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Actor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return 
        [
            'name'       => $this->faker->name,
            'birth_date' => $this->faker->date(),
            'gender'     => $this->faker->randomElement(GenderConst::GENDERS),
            'email'      => $this->faker->email(),
            'career_start_date' => $this->faker->date(),
            'career_end_date' => $this->faker->boolean() ? $this->faker->date() : null,
        ];
    }
}

# BV OTT Lumen Movie API

This is a small Laravel Lumen Movie API build based on a given OpenAPI Yaml Doc file. 

The Project includes a simple Docker-Compose Service, so that anyone can create and boot up the environment for the project easily.

## Docker environment

The Service is composed by 3 containers: 

| Container Name | Description |
| ------ | ------ |
|   Lumen     |    Container with webserver    |
|  movie-db   |  Container with MySQL Database  |
|  redis      |   Container with Redis Cache    |

 and one bridged network(`lumen-env`) to connect them all.

### To start up the Environment: 

1. Create a .env file based on .env.example (cp .env.example .env)
1. Run the command  `docker compose up -d` to build and boot up(in daemon) your environment.
1. After that, run the command `docker exec -it  lumen php artisan migrate` to run the migrations for the database tables.


An additional migration was created so that the project starts up with some populated data already.

Also, the project includes Factories for Movies, Directors and Actors to easily seed new data into the project.

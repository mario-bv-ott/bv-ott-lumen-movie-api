<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('country_movie', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->uuid('country_id');
            $table->foreign('country_id')->references('id')->on('country');
            $table->uuid('movie_id');
            $table->foreign('movie_id')->references('id')->on('movie');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('country_movie');
    }
};

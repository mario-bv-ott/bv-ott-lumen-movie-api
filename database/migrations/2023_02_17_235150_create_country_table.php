<?php

use Database\Seeders\CountryTableSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('country', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code_a2', 2)->unique();
            $table->string('code_a3', 3)->unique()->nullable();
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        //since is mostly a static data table, I've pushed up Country seed here too
        $seeder = new CountryTableSeeder();
        $seeder->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('country');
    }
};

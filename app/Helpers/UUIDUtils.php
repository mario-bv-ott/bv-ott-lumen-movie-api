<?php

namespace App\Helpers;

use Ramsey\Uuid\Uuid;

class UUIDUtils 
{
    /**
     * generateUUID function - Wrapper to generate
     *  a UUID V4 unique identifier
     *
     * @return string
     */
    public static function generateUUID(): string
    {
        return Uuid::uuid4();
    }
}